function plot_joint(qrecord,j)

figure(3)

subplot(2,4,1)
plot([1:j],qrecord(1,1:j));
xlim([1 j]);
ylim([-180 180]);

subplot(2,4,2)
plot([1:j],qrecord(2,1:j));
xlim([1 j]);
ylim([-180 180]);

subplot(2,4,3)
plot([1:j],qrecord(3,1:j));
xlim([1 j]);
ylim([-180 180]);

subplot(2,4,4)
plot([1:j],qrecord(4,1:j));
xlim([1 j]);
ylim([0 160]);

subplot(2,4,5)
plot([1:j],qrecord(5,1:j));
xlim([1 j]);
ylim([-180 180]);

subplot(2,4,6)
plot([1:j],qrecord(6,1:j));
xlim([1 j]);
ylim([-180 180]);

subplot(2,4,7)
plot([1:j],qrecord(7,1:j));
xlim([1 j]);
ylim([-180 180]);