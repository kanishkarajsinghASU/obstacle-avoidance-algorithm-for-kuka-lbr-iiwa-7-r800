function plot_joint(qrecord,j)

figure(5)
a = 3000;

subplot(2,4,1)
plot([1:j],qrecord(1,1:j));
xlim([1 j]);
ylim([-a a]);

subplot(2,4,2)
plot([1:j],qrecord(2,1:j));
xlim([1 j]);
ylim([-a a]);

subplot(2,4,3)
plot([1:j],qrecord(3,1:j));
xlim([1 j]);
ylim([-a a]);

subplot(2,4,4)
plot([1:j],qrecord(4,1:j));
xlim([1 j]);
ylim([-a a]);

subplot(2,4,5)
plot([1:j],qrecord(5,1:j));
xlim([1 j]);
ylim([-a a]);

subplot(2,4,6)
plot([1:j],qrecord(6,1:j));
xlim([1 j]);
ylim([-a a]);

subplot(2,4,7)
plot([1:j],qrecord(7,1:j));
xlim([1 j]);
ylim([-a a]);