function qdot = W(q)

qdot = zeros(7,1);

q1 = [170 -170 0]*pi/180;
q2 = [120 -120 0]*pi/180;
q3 = [170 -170 0]*pi/180;
q4 = [100 -100 0]*pi/180;
q5 = [170 -170 0]*pi/180;
q6 = [120 -120 0]*pi/180;
q7 = [175 -175 0]*pi/180;

qdot(1) = -q(1)/(7*(q1(1)-q1(2))^2);
qdot(2) = -q(2)/(7*(q2(1)-q2(2))^2);
qdot(3) = -q(3)/(7*(q3(1)-q3(2))^2);
qdot(4) = -q(4)/(7*(q4(1)-q4(2))^2);
qdot(5) = -q(5)/(7*(q5(1)-q5(2))^2);
qdot(6) = -q(6)/(7*(q6(1)-q6(2))^2);
qdot(7) = -q(7)/(7*(q7(1)-q7(2))^2);

qdot = 10*qdot;

    if(q(1)>170*pi/180||q(1)<-170*pi/180)
        fprintf('joint 1 hit a limit\n')
    end
    if(q(2)>120*pi/180||q(2)<-120*pi/180)
        fprintf('joint 2 hit a limit\n')
    end
    if(q(3)>170*pi/180||q(3)<-170*pi/180)
        fprintf('joint 3 hit a limit\n')
    end
    if(q(4)>120*pi/180||q(4)<-120*pi/180)
        fprintf('joint 4 hit a limit\n')
    end
    if(q(5)>170*pi/180||q(5)<-170*pi/180)
        fprintf('joint 5 hit a limit\n')
    end
    if(q(6)>120*pi/180||q(6)<-120*pi/180)
        fprintf('joint 6 hit a limit\n')
    end
    if(q(7)>175*pi/180||q(7)<-175*pi/180)
        fprintf('joint 7 hit a limit\n')
    end

