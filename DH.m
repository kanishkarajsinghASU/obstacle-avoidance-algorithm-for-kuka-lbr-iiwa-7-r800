function A = DH(d,t,a,al)
% syms d t a al;
A = [cos(t) -sin(t) 0 0;
    sin(t) cos(t) 0 0;
    0 0 1 d;
    0 0 0 1]*[1 0 0 a;
    0 cos(al) -sin(al) 0;
    0 sin(al) cos(al) 0;
    0 0 0 1];
% A = vpa(A,4);




