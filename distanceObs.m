function distanceObs(P)

%    P=[0 340 186 551 550;
%      0 0 312 377 377;
%      0 0 -197 -348 -222]

x =  550;
y = 0;

i = 0;
j = 0;

if((P(3,3)<0)&&(sqrt((x-P(1,3))^2+P(2,3)^2)<150))
    fprintf('joint 4 is hitting the obstacle\n');
    i = 1;
end

if((P(3,4)<0)&&(sqrt((x-P(1,4))^2+P(2,4)^2)<150))
    fprintf('joint 6 is hitting the obstacle\n');
    j = 1;
end

% if((i == 0)&&(j == 0))
%     fprintf('sir clear sir\n');
end