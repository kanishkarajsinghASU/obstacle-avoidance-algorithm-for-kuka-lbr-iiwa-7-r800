function plot_point(P,j,E)
%     P = [1 2 3 4 5 6 7;2 5 1 4 2 3 6;7 6 5 4 3 2 1]*100;
% j = 1;
% E = zeros(6,1);

a = [0.55, -0.1, -0.1]*1000;
b = [0.60, 0, -0.1]*1000;
c = [0.55, 0.1, -0.1]*1000;
d = [0.50, 0, -0.1]*1000;
e = [0.55, -0.1, -1.5]*1000;
f = [0.60, 0, -1.5]*1000;
g = [0.55, 0.1, -1.5]*1000;
h = [0.50, 0, -1.5]*1000;
    
   figure(1); 
   
   plot3([P(1,1) P(1,2)],[P(2,1) P(2,2)],[P(3,1) P(3,2)],'color','r');
   hold on;
   plot3([P(1,2) P(1,3)],[P(2,2) P(2,3)],[P(3,2) P(3,3)],'color','g');
   plot3([P(1,3) P(1,4)],[P(2,3) P(2,4)],[P(3,3) P(3,4)],'color','b');
   plot3([P(1,4) P(1,5)],[P(2,4) P(2,5)],[P(3,4) P(3,5)],'color','m');
   
   plot3([a(1) b(1)],[a(2) b(2)],[a(3) b(3)],'color','k');
   plot3([c(1) b(1)],[c(2) b(2)],[c(3) b(3)],'color','k');
   plot3([c(1) d(1)],[c(2) d(2)],[c(3) d(3)],'color','k');
   plot3([a(1) d(1)],[a(2) d(2)],[a(3) d(3)],'color','k');
   
   plot3([e(1) f(1)],[e(2) f(2)],[e(3) f(3)],'color','k');
   plot3([g(1) f(1)],[g(2) f(2)],[g(3) f(3)],'color','k');
   plot3([g(1) h(1)],[g(2) h(2)],[g(3) h(3)],'color','k');
   plot3([e(1) h(1)],[e(2) h(2)],[e(3) h(3)],'color','k');
   
   plot3([a(1) e(1)],[a(2) e(2)],[a(3) e(3)],'color','k');
   plot3([b(1) f(1)],[b(2) f(2)],[b(3) f(3)],'color','k');
   plot3([g(1) c(1)],[g(2) c(2)],[g(3) c(3)],'color','k');
   plot3([d(1) h(1)],[d(2) h(2)],[d(3) d(3)],'color','k');
   
   xlim([-700 1300]);
   ylim([-1000 1000]);
   zlim([-1600 1000]);

   grid on;
      
   xlabel('x-axis');
   ylabel('y-axis');
   zlabel('z-axis');   
%    view([0 0 1000]);
%     view([0 -1000 0]);
%     view([1000 0 0]);
   hold off;
end   
%    figure(2);
%    
%    subplot(2,3,1);
%    hold on
%    plot(j,E(1),'.','color','r');
%    xlim([0 3000]);
%    ylim([-20 20]);
%    
%    subplot(2,3,2);
%    hold on;
%    plot(j,E(2),'.','color','g');
%    xlim([0 3000]);
%    ylim([-20 20]);
%    
%    subplot(2,3,3);
%    hold on;
%    plot(j,E(3),'.','color','b');
%    xlim([0 3000]);
%    ylim([-20 20]);
%    
%    subplot(2,3,4);
%    hold on;
%    plot(j,E(4),'.','color','r');
%    xlim([0 3000]);
%    ylim([-0.1 0.1]);
%    
%    subplot(2,3,5);
%    hold on;
%    plot(j,E(5),'.','color','g');
%    xlim([0 3000]);
%    ylim([-0.1 0.1]);
%    
%    subplot(2,3,6);
%    hold on;
%    plot(j,E(6),'.','color','b');
%    xlim([0 3000]);
%    ylim([-0.1 0.1]);
%    
% end