    clear all;
    close all;
    tic
    Traj = zeros(3,1000);
    qrecord = zeros(7,8000);
    qvel = zeros(7,8000);
    qacc = zeros(7,8000);
    qprev = zeros(7,1);
    extentionx = 750;
    extentionz = 150;
    len1 = 300;
    len2 = 4000;
    
    pA = [584.12 -485.72 -232.06]';
    pB = [584.12 -485.72 extentionz]';
    pC = [extentionx -485.72 extentionz]';
    pD = [extentionx 377.42 extentionz]';
    pE = [550.20 377.42 extentionz]';
    pF = [550.20 377.42 -222.19]';
 
    Zcentre = -80;
    Ycentre = (pC(2)+pD(2))/2;
    centre = [pC(1) Ycentre Zcentre]';
    radius = sqrt((centre(1)-pC(1))^2+(centre(2)-pC(2))^2+(centre(3)-pC(3))^2);
    
    theta1 = atan2(pC(3)-Zcentre,pC(2)-Ycentre);
    theta2 = atan2(pD(3)-Zcentre,pD(2)-Ycentre);
    
%     PM1 =[pC(1),centre(2)+radius*cos(theta(4)),centre(3)+radius*cos(theta(4))]';
%     PM1 =[pC(1),centre(2)+radius*cos(theta(6)),centre(3)+radius*cos(theta(6))]';
    
    
    thetaI = linspace(theta1,theta2,len1);
    theta3 = thetaI((round(len1/3)));
    theta4 = thetaI(round(2*len1/3));
    
    the1 = linspace(theta1,theta3,len1);
    the2 = linspace(theta3,theta4,len2);
    the3 = linspace(theta4,theta2,len1);   
    
    %going from A to B
    Traj(1,1:len1) = linspace(pA(1),pB(1),len1);
    Traj(2,1:len1) = linspace(pA(2),pB(2),len1);
    Traj(3,1:len1) = linspace(pA(3),pB(3),len1);
    
    %going from B to C
    Traj(1,len1+1:2*len1) = linspace(pB(1),pC(1),len1);
    Traj(2,len1+1:2*len1) = linspace(pB(2),pC(2),len1);
    Traj(3,len1+1:2*len1) = linspace(pB(3),pC(3),len1);
        
    %going from C to D
    Traj(1,2*len1+1:3*len1) = pC(1)*ones(1,len1);
    Traj(2,2*len1+1:3*len1) = centre(2)+radius*cos(the1);
    Traj(3,2*len1+1:3*len1) = centre(3)+radius*sin(the1);
    
    Traj(1,3*len1+1:3*len1+len2) = pC(1)*ones(1,len2);
    Traj(2,3*len1+1:3*len1+len2) = centre(2)+radius*cos(the2);
    Traj(3,3*len1+1:3*len1+len2) = centre(3)+radius*sin(the2);
    
    Traj(1,3*len1+len2+1:4*len1+len2) = pC(1)*ones(1,len1);
    Traj(2,3*len1+len2+1:4*len1+len2) = centre(2)+radius*cos(the3);
    Traj(3,3*len1+len2+1:4*len1+len2) = centre(3)+radius*sin(the3);
        
    %going from D to E
    Traj(1,4*len1+len2+1:5*len1+len2) = linspace(pD(1),pE(1),len1);
    Traj(2,4*len1+len2+1:5*len1+len2) = linspace(pD(2),pE(2),len1);
    Traj(3,4*len1+len2+1:5*len1+len2) = linspace(pD(3),pE(3),len1);
    
    %going from E to F
    Traj(1,5*len1+len2+1:6*len1+len2) = linspace(pE(1),pF(1),len1);
    Traj(2,5*len1+len2+1:6*len1+len2) = linspace(pE(2),pF(2),len1);
    Traj(3,5*len1+len2+1:6*len1+len2) = linspace(pE(3),pF(3),len1);
    
    
    
    qA = [-109.19 80.81 64.66 70.93 0.25 -88.23 63.10]'*pi/180;
    qB = [-53.41 112.63 -13.84 92.16 -41.09 -97.44 -23.31]'*pi/180;
    
    q = qA;
    qprev = qA;
    
    j = 0;

    K = 0.08*eye(6);
    K(4,4) = 0.001;
    K(6,6) = 0.001;
    
    [Ae,Je,ee,P] = kin_jac(q);
    e = zeros(1,6);
%     plot_point(P,1,e);
%     pause(5);
    
for i = 1:(6*len1+len2)
    Xd = [Traj(:,i);ee'];
    [Ae,Je,ee,P] = kin_jac(q);
    Xe = [Ae(1:3,4);[ee]'];
    e = Xd - Xe;
while((max(abs(e(1:3)))>0.1)||max(abs(e(4:6)))>0.01)
    j = j+1;
    [Ae,Je,ee,P] = kin_jac(q);
    Xe = [Ae(1:3,4);[ee]'];
    e = Xd - Xe;
    qdot = pinv(Je)*K*e + (eye(7)-pinv(Je)*Je)*W(q);

    q = q + qdot;
    
    distanceObs(P);
   
%     plot_point(P,j,e);
%     pause(0.000001);
    
end

    qvel(:,i) = (q-qprev)*1000/5;
    qprev = q;
    qacc(:,i) = (qvel(:,i+1) - qvel(:,i))*1000/5;
    qrecord(:,i) = q;

end

qrecord = qrecord*180/pi;
qvel = qvel*180/pi;
qacc = qvel*180/pi;
plot_joint(qrecord,i);

plot_vel(qvel,i);

plot_acc(qacc,i);

toc